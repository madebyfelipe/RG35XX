#!/system/bin/sh

BN="/system/bin/"
BB="/system/bin/busybox"
CO="/system/data/compat/"

# Set the parent directories containing ROM directories
SD1="/mnt/mmc/ROMS"
SD2="/mnt/sdcard/ROMS"

PLAYLIST_LOCATION="/mnt/mmc/LIST"
DATABASE_LOCATION="/mnt/mmc/DATA"

CORE_MAPPING_FILE="/system/data/compat/coremapping.json"
MAME_MAPPING_FILE="/system/data/compat/mame.json"

# Add more core names here as needed - not sure how many 'mame' cores there are?
CUSTOM_LABEL_CORES="ARCADE FBA2012 FBNEO MAME2000 NEOGEO"

DB_DESC="muOS Custom Database"
DB_DATE=$("$BB" date +'%Y.%-1m.%-1d')

GOVERNOR="/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"
PREV_GOV=$("$BB" cat "$GOVERNOR")

# Better to boost it to performance if we're on a different governor
"$BB" echo performance > "$GOVERNOR"

# Path to the temporary CRC32 file
FAKECRC_TEMP="/mnt/mmc/MUOS/.checksum"

# Check if the temporary file exists, and if not, create it
[ ! -e "$TEMPFILE" ] && busybox touch "$TEMPFILE"

# Generate a random alphanumeric string of length 8
FAKECRC() {
    "$BB" tr -dc 'A-F0-9' < /dev/urandom | "$BB" head -c 8
}

# Loop through cores in the core mapping file
for CORE_NAME in $("$BN"jq -r 'keys[]' "$CORE_MAPPING_FILE"); do
    CORE_DATA=$("$BN"jq --arg CORE_NAME "${CORE_NAME}" -r ".[\$CORE_NAME]" "$CORE_MAPPING_FILE")
    CORE_FILENAME=$("$BB" echo "$CORE_DATA" | "$BN"jq -r '.[0]')
    CORE_LABEL=$("$BB" echo "$CORE_DATA" | "$BN"jq -r '.[1]')

    # Get a total count of files on both SD cards
    NUM_ROMS_SD1=$("$BB" find "$SD1/$CORE_NAME" -maxdepth 1 -type f 2>/dev/null | "$BB" wc -l)
    NUM_ROMS_SD2=$("$BB" find "$SD2/$CORE_NAME" -maxdepth 1 -type f 2>/dev/null | "$BB" wc -l)
    TOTAL_NUM_ROMS=$((NUM_ROMS_SD1 + NUM_ROMS_SD2))
    CURRENT_FILE=0

    PLAYLIST_NAME="$PLAYLIST_LOCATION/$CORE_LABEL.lpl"
    DATABASE_NAME="$DATABASE_LOCATION/$CORE_LABEL.rdb"
    DATABASE_TEMP="$DATABASE_LOCATION/$CORE_LABEL.dat"

    if [ "$1" = "overwrite" ]; then
        [ -f "$PLAYLIST_NAME" ] && "$BB" rm -f "$PLAYLIST_NAME"
        [ -f "$DATABASE_NAME" ] && "$BB" rm -f "$DATABASE_NAME"
        [ -f "$DATABASE_TEMP" ] && "$BB" rm -f "$DATABASE_TEMP"
    elif [ -f "$PLAYLIST_NAME" ]; then
        continue
    fi

    PROCROM_DIR() {
        ROM_DIRECTORY="$1"

        # If the ROM directory specified in core mapping does not exist, move on
        [ ! -d "$ROM_DIRECTORY" ] && return

        # If the ROM count equals zero for either SD1 or SD2, then move on
        if [ "$2" = "SD1" ] && [ "$NUM_ROMS_SD1" -eq 0 ] || [ "$2" = "SD2" ] && [ "$NUM_ROMS_SD2" -eq 0 ]; then return; fi

        if [ ! -f "$PLAYLIST_NAME" ]; then
            # Create the beginning of the playlist - might need to adjust this for different label displays (to remove brackets and stuff)
            PLAYLIST='{"version":"1.5","default_core_path":"/mnt/mmc/CORE/'$CORE_FILENAME'","default_core_name":"'$CORE_LABEL'","base_content_directory":"/mnt/","label_display_mode":3,"right_thumbnail_mode":0,"left_thumbnail_mode":0,"sort_mode":1,"scan_content_dir":"'$ROM_DIRECTORY'","scan_file_exts":"","scan_dat_file_path":"","scan_search_recursively":false,"scan_search_archives":true,"scan_filter_dat_content":false,"items":['
            "$BB" echo "$PLAYLIST" > "$PLAYLIST_NAME"

            # Create the beginning of the database file
            "$BB" printf 'clrmamepro (\n\tname "%s"\n\tdescription "%s"\n\tversion "%s"\n)\n\n' "$CORE_LABEL" "$DB_DESC" "$DB_DATE" > "$DATABASE_TEMP"
        else
            # If the ROM count for SD2 is zero, then just move on; otherwise, continue adding to the playlist
            if [ "$2" = "SD2" ] && [ "$NUM_ROMS_SD2" -eq 0 ]; then return; fi
        fi

        # Loop through ROMs in the directory and append items for the core
        for ROM_FILE in "$ROM_DIRECTORY"/*; do
            if [ -f "$ROM_FILE" ]; then
                ROM_NAME=${ROM_FILE##*/}
                ROM_LABEL=${ROM_NAME%.*}
                ROM_EXT=$("$BB" echo "${ROM_FILE##*.}" | "$BB" tr '[:upper:]' '[:lower:]')

                # Skip the neogeo BIOS file, it should exist, but not in a playlist!
                [ "$ROM_LABEL.$ROM_EXT" = "neogeo.zip" ] && continue

                # Create a CRC32 checksum for each of the files
                CHECKSUM=$(FAKECRC)
                while "$BN"grep -q "$CHECKSUM" "$FAKECRC_TEMP"; do
                    CHECKSUM=$(FAKECRC)
                done
                "$BB" echo "$CHECKSUM" >> "$TEMPFILE"

                # Check if the CORE_NAME requires custom labeling due to it being in mame.json
                if "$BB" printf '%s\n' "$CUSTOM_LABEL_CORES" | "$BN"grep -q -w "$CORE_NAME"; then
                    CUSTOM_LABEL=$("$BN"jq --arg ROM_LABEL "${ROM_LABEL}" -r ".[\$ROM_LABEL]" "$MAME_MAPPING_FILE")
                    if [ "$CUSTOM_LABEL" != "null" ]; then
                        ROM_LABEL="$CUSTOM_LABEL"
                    fi
                else
                    # If the file is a compressed file, grab the first file within and use that as the ROM file
                    if { [ "$ROM_EXT" = "zip" ] || [ "$ROM_EXT" = "7z" ]; }; then
                        COMPRESSED_FILE=$("$BN"7za l -ba -slt "$ROM_FILE" | "$BB" awk '/Path =/ {gsub(/Path = /, ""); print}')
                        ROM_FILE="$ROM_FILE#$COMPRESSED_FILE"
                    fi
                fi

                # Now process each of the roms and add them to the playlist
                PL_ROM_ITEM='{"path":"'$ROM_FILE'","label":"'$ROM_LABEL'","core_path":"DETECT","core_name":"DETECT","crc32":"'$CHECKSUM'|crc","db_name":"'$CORE_LABEL'.lpl"}'
                "$BB" sed -i "\$a$PL_ROM_ITEM," "$PLAYLIST_NAME"

                # And now add it to the database file
                "$BB" printf 'game (\n\ttitle "%s"\n\trom ( name "%s" crc %s )\n)\n\n' "$ROM_LABEL" "$ROM_LABEL" "$CHECKSUM" >> "$DATABASE_TEMP"
            fi

            # Increment the current file number and print progress
            CURRENT_FILE=$((CURRENT_FILE + 1))
            PERCENTAGE=$((CURRENT_FILE * 100 / TOTAL_NUM_ROMS))

            "$BB" printf "%d\tProcessing %s\n" "$PERCENTAGE" "$CORE_NAME"
        done

        return
    }

    # Process ROM directories for both SD cards
    PROCROM_DIR "$SD1/$CORE_NAME" "SD1"
    PROCROM_DIR "$SD2/$CORE_NAME" "SD2"

    if [ -f "$PLAYLIST_NAME" ]; then
        # Remove the last comma before the closing bracket
        "$BB" sed -i "\$s/,\$//" "$PLAYLIST_NAME"

        # Close the playlist structure
        "$BB" sed -i "\$a]}" "$PLAYLIST_NAME"

        # Use jq to compact and tidy up the JSON file
        "$BN"jq . "$PLAYLIST_NAME" > "${PLAYLIST_NAME}.tmp" && "$BB" mv "${PLAYLIST_NAME}.tmp" "$PLAYLIST_NAME"

        "$BB" printf "100\tPlaylist created for %s\n" "$CORE_NAME"
        "$BB" sleep 1
    fi

    if [ -f "$DATABASE_TEMP" ]; then
        # Convert the temporary database into a RetroArch database
        "$CO"dat2rdb "${DATABASE_NAME}" "${DATABASE_TEMP}"

        "$BB" printf "100\tDatabase created for %s\n" "$CORE_NAME"
        "$BB" sleep 1

        # Let's hope that the RetroArch database has been created...
        if [ -f "$DATABASE_NAME" ]; then
            "$BB" rm -f "$DATABASE_TEMP"
        fi
    fi
done

"$BB" echo "$PREV_GOV" > "$GOVERNOR"
"$BB" printf "100\tRetroArch Playlists Updated\n"