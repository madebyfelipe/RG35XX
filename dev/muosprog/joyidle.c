#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <linux/input.h>
#include <linux/joystick.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include "options.h"

int js_fd;
int is_idle;
int idle_timeout;
int idle_shutdown;

void set_bl_power(int power) {
    FILE* file = fopen(BL_POWER_FILE, "w");
    if (file != NULL) {
        fprintf(file, "%d", power);
        fclose(file);
    } else {
        perror("Failed to open bl_power file");
        exit(1);
    }
}

void set_spk_power(char* power) {
    char command[256];
    snprintf(command, sizeof(command), "%s -q set '%s' %s", VOL_ALSA_MIXER, VOL_SPK_SWITCH, power);
    system(command);
}

void handle_idle_timeout() {
    set_bl_power(1);
    set_spk_power("off");
    is_idle = 1;
}

void handle_shutdown_timeout() {
    system("sync && busybox poweroff -f");
}

void handle_button_press() {
    set_bl_power(0);
//    set_spk_power("on");
    is_idle = 0;
}

void monitor_joystick_activity() {
    struct js_event js_event;
    time_t last_active_time = time(NULL);
    int idle_time = 0;
    fd_set read_fds;
    int max_fd = js_fd + 1;

    while (1) {
        FD_ZERO(&read_fds);
        FD_SET(js_fd, &read_fds);

        struct timeval timeout;
        timeout.tv_sec = 1;
        timeout.tv_usec = 0;

        int ret = select(max_fd, &read_fds, NULL, NULL, &timeout);

        if (ret == -1) {
            perror("Error in select");
            exit(1);
        } else if (ret == 0) {
            if (idle_time >= idle_timeout && !is_idle)
                handle_idle_timeout();
            if (idle_time >= idle_shutdown && is_idle)
                handle_shutdown_timeout();
            idle_time = time(NULL) - last_active_time;
            continue;
        }

        if (FD_ISSET(js_fd, &read_fds)) {
            int ret = read(js_fd, &js_event, sizeof(struct js_event));
            if (ret == -1) {
                perror("Error reading joystick event");
                exit(1);
            }
            time_t current_time = time(NULL);
            last_active_time = current_time;
            handle_button_press();
        }
    }
}

void setup_background_process() {
    pid_t pid = fork();

    if (pid == -1) {
        perror("Failed to fork");
        exit(1);
    } else if (pid > 0) {
        exit(0);
    }
}

void open_joystick_device() {
    js_fd = open(JOY_DEVICE, O_RDONLY | O_NONBLOCK);

    if (js_fd == -1) {
        perror("Failed to open joystick device");
        exit(1);
    }
}

int main(int argc, char *argv[]) {
    setup_background_process();
    open_joystick_device();

    idle_timeout = ID_SHUTDOWN;
    idle_shutdown = ID_TIMEOUT;

    if (argc > 2) {
        idle_timeout = atoi(argv[1]);
        idle_shutdown = atoi(argv[2]);
    }

    monitor_joystick_activity();
}
