#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <linux/input.h>
#include <linux/joystick.h>
#include "options.h"

#define JOY_HOTKEY	JOY_SELECT

int js_fd;
int hotkeyPressed = 0;

int read_brightness() {
    FILE* file = fopen(BL_BRIGHT_FILE, "r");
    if (file != NULL) {
        int brightness;
        fscanf(file, "%d", &brightness);
        fclose(file);
        return brightness;
    } else {
        perror("Failed to open brightness file");
        return 192;
    }
}

void set_brightness(int brightness) {
    FILE* file = fopen(BL_BRIGHT_FILE, "w");
    if (file != NULL) {
        fprintf(file, "%d", brightness);
        fclose(file);
    } else {
        perror("Failed to open brightness file");
    }
}

void save_brightness(int brightness) {
    FILE* file = fopen(BL_RST_FILE, "w");
    if (file != NULL) {
        fprintf(file, "%d", brightness);
        fclose(file);
    } else {
        perror("Failed to open brightness restore file");
        exit(1);
    }
}

void set_bl_power(int power) {
    FILE* file = fopen(BL_POWER_FILE, "w");
    if (file != NULL) {
        fprintf(file, "%d", power);
        fclose(file);
    } else {
        perror("Failed to open bl_power file");
    }
}

void read_joystick_events() {
    struct js_event ev;
    int    newBrightness = -1;

    while (1) {
        int currentBrightness = read_brightness();

        if (newBrightness == -1)
            newBrightness = currentBrightness;

        read(js_fd, &ev, sizeof(struct js_event));

        switch (ev.type) {
            case JS_EVENT_BUTTON:
                if (ev.value == 1) {
                    if (ev.number == JOY_HOTKEY)
                        hotkeyPressed = 1;
                    if (hotkeyPressed == 1 && ev.number == JOY_PLUS) {
                        if (currentBrightness <= 17) {
                            newBrightness = currentBrightness + 1;
                            printf("up 1 :: %d\n", newBrightness);
                        } else {
                            newBrightness = currentBrightness + BL_INC;
                        }
                        newBrightness = (newBrightness > BL_MAX) ? BL_MAX : newBrightness;
                    }
                    if (hotkeyPressed == 1 && ev.number == JOY_MINUS) {
                        if (currentBrightness <= 17) {
                            newBrightness = currentBrightness - 1;
                            printf("down 1 :: %d\n", newBrightness);
                        } else {
                            newBrightness = currentBrightness - BL_INC;
                        }
                        newBrightness = (newBrightness < BL_MIN) ? BL_MIN : newBrightness;
                    }
                } else if (ev.value == 0 && ev.number == JOY_HOTKEY)
                    hotkeyPressed = 0;
                break;
            default:
                break;
        }

        if (hotkeyPressed == 1 && newBrightness != currentBrightness) {
            set_brightness(newBrightness);
            save_brightness(newBrightness);
            set_bl_power(newBrightness == 0 ? 1 : 0);
        }

        usleep(180000);
    }
}

void open_joystick_device() {
    js_fd = open(JOY_DEVICE, O_RDONLY | O_NONBLOCK);

    if (js_fd == -1) {
        perror("Failed to open joystick device");
        exit(1);
    }
}

void restore_brightness_settings() {
    int brightnessFromFile;

    FILE *file = fopen(BL_RST_FILE, "r");
    if (file != NULL) {
        fscanf(file, "%d", &brightnessFromFile);
        fclose(file);
        if (brightnessFromFile == 0)
            brightnessFromFile = BL_DEF;
        else
            brightnessFromFile = (brightnessFromFile > BL_MAX) ? BL_MAX : brightnessFromFile;
        set_brightness(brightnessFromFile);
    } else {
        set_brightness(BL_DEF);
    }
}

int main() {
    restore_brightness_settings();
    open_joystick_device();
    read_joystick_events();
}
