#include <linux/input.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <linux/joystick.h>
#include <sys/ioctl.h>
#include <sys/epoll.h>
#include <sys/time.h>
#include <sys/types.h>
#include "options.h"

int js_fd;
int is_sleep = 0;
int epoll_fd;

pid_t pid;

void setup_epoll() {
    epoll_fd = epoll_create1(0);
    if (epoll_fd == -1) {
        perror("Failed to create epoll instance");
        exit(1);
    }

    struct epoll_event event;
    event.events = EPOLLIN;
    event.data.fd = js_fd;

    if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, js_fd, &event) == -1) {
        perror("Failed to add file descriptor to epoll instance");
        exit(1);
    }
}

void set_bl_power(int power) {
    FILE* file = fopen(BL_POWER_FILE, "w");
    if (file != NULL) {
        fprintf(file, "%d", power);
        fclose(file);
    } else {
        perror("Failed to open bl_power file");
        exit(1);
    }
}

void set_governor(char* governor) {
    FILE* file = fopen(GOVERNOR_FILE, "w");
    if (file != NULL) {
        fprintf(file, "%s", governor);
        fclose(file);
    } else {
        perror("Failed to open scaling_governor file");
        exit(1);
    }
}

void set_cpu_scale(int speed) {
    FILE* file = fopen(SCALE_MX_FILE, "w");
    if (file != NULL) {
        fprintf(file, "%d", speed);
        fclose(file);
    } else {
        perror("Failed to open scaling_max_freq file");
        exit(1);
    }
}

void set_spk_power(char* power) {
    char command[256];
    snprintf(command, sizeof(command), "%s -q set '%s' %s", VOL_ALSA_MIXER, VOL_SPK_SWITCH, power);
    system(command);
}

void kill_process_by_name(char* process_name) {
    char command[256];
    snprintf(command, sizeof(command), "/bin/busybox killall %s", process_name);
    if (system(command) == -1) {
        perror("Error executing killall");
    }
}

void restart_joyidle() {
    FILE *file = fopen("/mnt/mmc/MUOS/.muosopt", "r");
    if (file == NULL) {
        perror("Failed to open configuration file");
    }

    int blank_value = -1;
    int shutdown_value = -1;

    char line[256];
    while (fgets(line, sizeof(line), file)) {
        if (strncmp(line, "BLANK=", 6) == 0) {
            blank_value = atoi(line + 6);
        } else if (strncmp(line, "SHUTDOWN=", 9) == 0) {
            shutdown_value = atoi(line + 9);
        }

        if (blank_value != -1 && shutdown_value != -1) {
            break;
        }
    }

    fclose(file);

    char command[256];
    snprintf(command, sizeof(command), "/usr/bin/joyidle %d %d", blank_value, shutdown_value);
    if (system(command) == -1) {
        perror("Error executing joyidle process");
    }
}

void read_joystick_events() {
    struct js_event ev;

    int epoll_fd = epoll_create1(0);
    if (epoll_fd == -1) {
        perror("Failed to create epoll instance");
        exit(1);
    }

    struct epoll_event event;
    event.events = EPOLLIN;
    event.data.fd = js_fd;

    if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, js_fd, &event) == -1) {
        perror("Failed to add file descriptor to epoll instance");
        exit(1);
    }

    struct timeval start_time, current_time;
    gettimeofday(&start_time, NULL);

    while (1) {
        struct epoll_event events[1];
        int num_events = epoll_wait(epoll_fd, events, 1, -1);
        if (num_events == -1) {
            perror("Failed to wait for events using epoll");
            exit(1);
        }

        read(js_fd, &ev, sizeof(struct js_event));

        if (ev.type == JS_EVENT_BUTTON && ev.number == JS0_POWER) {
            if (ev.value == 1) {
                gettimeofday(&current_time, NULL);
                double elapsed_time = (current_time.tv_sec - start_time.tv_sec) +
                                      (current_time.tv_usec - start_time.tv_usec) / 1000000.0;
                if (elapsed_time <= 1.3) {
                    if (is_sleep == 0) {
                        is_sleep = 1;
                        kill_process_by_name("/usr/bin/joyidle");
                        set_bl_power(1);
                        set_spk_power("off");
                        set_governor("powersave");
                        set_cpu_scale(388000);
                        sleep(1);
                    } else {
                        is_sleep = 0;
                        restart_joyidle();
                        set_bl_power(0);
                        set_spk_power("on");
                        set_governor("userspace");
                        set_cpu_scale(1388000);
                        sleep(1);
                    }
                } else {
                    gettimeofday(&start_time, NULL);
                }
            }
        }
    }

    close(js_fd);
    epoll_ctl(epoll_fd, EPOLL_CTL_DEL, js_fd, NULL);
}

void open_joystick_device() {
    js_fd = open(JOY_DEVICE, O_RDWR | O_NONBLOCK);

    if (js_fd == -1) {
        perror("Failed to open joystick device");
        exit(1);
    }
}

void setup_background_process() {
    pid_t pid = fork();

    if (pid == -1) {
        perror("Failed to fork");
        exit(1);
    } else if (pid > 0) {
        // printf("Background process started with PID: %d\n", pid);
        exit(0);
    }
}

int main(int argc, char *argv[]) {
    setup_background_process();
    open_joystick_device();
    setup_epoll();
    read_joystick_events();
}
