#define JOY_DEVICE     "/dev/input/js0"
#define JOY_EVENT      "/dev/input/event1"

#define GOVERNOR_FILE  "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"
#define SCALE_MX_FILE  "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

#define RUM_DEVICE     "/sys/class/power_supply/battery/moto"

#define BL_BRIGHT_FILE "/sys/class/backlight/backlight.2/brightness"
#define BL_POWER_FILE  "/sys/class/backlight/backlight.2/bl_power"
#define BL_RST_FILE    "/mnt/mmc/MUOS/.brightness"

#define VOL_ALSA_MIXER "/usr/bin/amixer"
#define VOL_RST_FILE   "/mnt/mmc/MUOS/.volume"

#define VOL_SPK_MASTER "DAC PA"
#define VOL_SPK_SWITCH "speaker on off switch" // yes really...
#define VOL_SPK_LEFT   "DAC FL Gain"
#define VOL_SPK_RIGHT  "DAC FR Gain"

#define OPTION_FILE    "/mnt/mmc/MUOS/.muosopt"
#define OPTION_MISC    "/misc/options.txt"

#define ID_TIMEOUT  300
#define ID_SHUTDOWN 600

#define JOY_A       0
#define JOY_B       1
#define JOY_X       2
#define JOY_Y       3
#define JOY_POWER   4
#define JOY_L1      5
#define JOY_R1      6
#define JOY_SELECT  7
#define JOY_START   8
#define JOY_MENU    9
#define JOY_PLUS    10
#define JOY_MINUS   11

#define JOY_UP      7
#define JOY_DOWN    7
#define JOY_LEFT    6
#define JOY_RIGHT   6
#define JOY_L2      2
#define JOY_R2      5

#define JS0_POWER   4
#define EV1_POWER   309

#define BL_DEF      384
#define BL_MAX      1024
#define BL_MIN      0
#define BL_INC      16

#define VOL_DEF	    22
#define VOL_MAX	    40
#define VOL_MIN	    0
#define VOL_INC	    2
