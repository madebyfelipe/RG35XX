## JoyAudio - Hardware Level Audio Scaler

This little program will adjust ALSA at the hardware level.
In muOS RetroArch is set to -15.0f audio level as baseline.
Upon starting this program the audio will restore both speakers to 100% as they are separate to master volume.
0% master volume or muting directly will switch the speaker off completely.

* `VOLUME UP (PLUS)` - Raises the volume of the speaker.
* `VOLUME DOWN (MINUS)` - Lowers the volume of the speaker.
* `SELECT + DOWN` - Mutes the speaker.

## JoyBright - Incremental Brightness Scaler

This little program will adjust the backlight of the RG35XX.
Upon starting this program the level will be restored based on how it was upon shutdown.
Turning the brightness to the lowest level will turn the screen off completely.
Because of the increment used (16) there is a nice low level brightness for night time.

* `SELECT + UP` - Increases the brightness until max level is hit (1024).
* `SELECT + DOWN` - Decreases the brightness level.

## JoyIdle - Idle input detector and shut down tool

This little program detects idle input and will shut down as required

If no other arguments are specified JoyIdle will set the following:
* 5 Minutes (300 seconds) idle for the screen to go blank and mute the audio
* 10 Minutes (600 seconds) idle will sync the SD Card and shut it down safely.

You can change those two parameters like so:
`joyidle 400 800`

JoyIdle does not care about your current game and will **not** save it.

## JoySleep - A simplified sleep mechanism

This little program will place the device in sleep/wake mode if you double-tap the power button.
The double-tap is not a _fast_ tap, there is about a 0.5-0.75 second delay to make sure there are no accidents in activating it.

* `POWER (double-tap)` - Will place the device in sleep/wake mode.
