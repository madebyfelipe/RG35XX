#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/joystick.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include "options.h"

int js_fd;
int png_index = 0;
int volup_pressed = 0;
int power_pressed = 0;
int queue_size = 0;

char storage_location[64];
char command_queue[128][128];

void set_rumble_level(int level) {
    char data[4];

    snprintf(data, sizeof(data), "%d", level);
    FILE* file = fopen(RUM_DEVICE, "w");

    if (file) {
        fwrite(data, sizeof(char), strlen(data), file);
        fclose(file);
    }
}

void open_joystick_device() {
    js_fd = open(JOY_DEVICE, O_RDONLY | O_NONBLOCK);

    if (js_fd == -1) {
        perror("Failed to open joystick device");
        exit(1);
    }
}

void process_command_queue() {
    for (int i = 0; i < queue_size; i++) {
        set_rumble_level(25);
        system(command_queue[i]);
        set_rumble_level(0);
    }
    queue_size = 0;
}

void add_command_to_queue(const char* command) {
    if (queue_size < 128) {
        strncpy(command_queue[queue_size], command, sizeof(command_queue[queue_size]));
        queue_size++;
    } else {
        fprintf(stderr, "Command queue is full. Skipping command: %s\n", command);
    }
}

void read_joystick_events() {
    struct js_event ev;
    char time_str[16];
    char command[128];
    time_t current_time;
    struct tm* time_info;

    int epoll_fd = epoll_create1(0);
    if (epoll_fd == -1) {
        perror("Failed to create epoll instance");
        exit(1);
    }

    struct epoll_event event;
    event.events = EPOLLIN;
    event.data.fd = js_fd;

    if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, js_fd, &event) == -1) {
        perror("Failed to add file descriptor to epoll instance");
        exit(1);
    }

    char prev_time_str[16] = ""; // Initialize to an empty string

    while (1) {
        struct epoll_event events[1];
        int num_events = epoll_wait(epoll_fd, events, 1, -1);
        if (num_events == -1) {
            perror("Failed to wait for events using epoll");
            exit(1);
        }

        read(js_fd, &ev, sizeof(struct js_event));

        current_time = time(NULL);
        time_info = localtime(&current_time);
        strftime(time_str, sizeof(time_str), "%Y%m%d_%H%M", time_info);

        if (strcmp(time_str, prev_time_str) != 0) {
            strcpy(prev_time_str, time_str);
            png_index = 0; // Reset png_index when time_str changes
        }

        switch (ev.type) {
            case JS_EVENT_BUTTON:
                if (ev.number == JOY_PLUS) {
                    volup_pressed = ev.value;
                }
                if (ev.number == JOY_POWER) {
                    power_pressed = ev.value;
                }
                break;
            default:
                break;
        }

        if (volup_pressed && power_pressed) {
            if (queue_size < 128) {
                snprintf(command, sizeof(command), "/usr/bin/fbgrab -z 9 /mnt/%s/%s_%s_%d.png >/dev/null 2>&1", storage_location, "muOS", time_str, png_index);
                add_command_to_queue(command);
                png_index++;
            } else {
                fprintf(stderr, "Command queue is full. Skipping command creation.\n");
            }
            process_command_queue();
        }
    }
}

void setup_background_process() {
    pid_t pid = fork();

    if (pid == -1) {
        perror("Failed to fork");
        exit(1);
    } else if (pid > 0) {
        // printf("Background process started with PID: %d\n", pid);
        exit(0);
    }
}

int main(int argc, char* argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <mmc/sdcard>\n", argv[0]);
        exit(1);
    }

    strncpy(storage_location, argv[1], sizeof(storage_location));
    setup_background_process();
    open_joystick_device();
    read_joystick_events();
}
