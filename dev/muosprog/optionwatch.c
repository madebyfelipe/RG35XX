#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/inotify.h>
#include "options.h"

int file_watcher() {
    int fd = inotify_init();
    if (fd == -1) {
        perror("inotify_init");
        return EXIT_FAILURE;
    }

    int wd = inotify_add_watch(fd, OPTION_FILE, IN_MODIFY);
    if (wd == -1) {
        perror("inotify_add_watch");
        close(fd);
        return EXIT_FAILURE;
    }

    while (1) {
        char buffer[4096];
        int length = read(fd, buffer, sizeof(buffer));
        if (length == -1) {
            perror("read");
            close(fd);
            return EXIT_FAILURE;
        }

        struct inotify_event *event = (struct inotify_event *) buffer;
        if (event->mask & IN_MODIFY) {
            FILE *sourceFile = fopen(OPTION_FILE, "r");
            if (sourceFile == NULL) {
                perror("fopen (source)");
                close(fd);
                return EXIT_FAILURE;
            }

            FILE *destinationFile = fopen(OPTION_MISC, "w");
            if (destinationFile == NULL) {
                perror("fopen (destination)");
                fclose(sourceFile);
                close(fd);
                return EXIT_FAILURE;
            }

            int ch;
            while ((ch = fgetc(sourceFile)) != EOF) {
                fputc(ch, destinationFile);
            }

            fclose(sourceFile);
            fclose(destinationFile);
        }
    }

    inotify_rm_watch(fd, wd);
    close(fd);

    return EXIT_SUCCESS;
}

int main() {
    file_watcher();

    return EXIT_SUCCESS;
}
