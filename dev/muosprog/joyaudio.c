#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <linux/input.h>
#include <linux/joystick.h>
#include "options.h"

#define JOY_HOTKEY JOY_SELECT

int js_fd;
int hotkeyPressed = 0;

int get_master_volume() {
    FILE* fp;

    char output[256];
    char volume_str[4];
    char command[256];

    snprintf(command, sizeof(command), "%s get '%s'", VOL_ALSA_MIXER, VOL_SPK_MASTER);

    fp = popen(command, "r");
    if (fp == NULL) {
        printf("Failed to run amixer command.\n");
        return 1;
    }

    while (fgets(output, sizeof(output), fp) != NULL) {
        if (strncmp(output, "  Mono:", 7) == 0) {
            sscanf(output, "  Mono: %2s", volume_str);
            break;
        }
    }

    pclose(fp);

    return atoi(volume_str);
}

void save_volume(int volume) {
    FILE* file = fopen(VOL_RST_FILE, "w");
    if (file != NULL) {
        fprintf(file, "%d", volume);
        fclose(file);
    } else {
        perror("Failed to open volume restore file");
        exit(1);
    }
}

void execute_mixer(int type, char* control, void* data) {
    char command[256];
    if (type == 0) {
        int value = *((int*)data);
        snprintf(command, sizeof(command), "%s -q set '%s' %d", VOL_ALSA_MIXER, control, value);
    } else if (type == 1) {
        char* value = (char*)data;
        snprintf(command, sizeof(command), "%s -q set '%s' %s", VOL_ALSA_MIXER, control, value);
    }
    system(command);
}

void read_joystick_events() {
    struct js_event ev;
    int    newVolume = -1;

    while (1) {
        int currentVolume = get_master_volume();

        if (newVolume == -1)
            newVolume = currentVolume;

        read(js_fd, &ev, sizeof(struct js_event));

        switch (ev.type) {
            case JS_EVENT_BUTTON:
                if (ev.value == 1) {
                    if (ev.number == JOY_HOTKEY) {
                        hotkeyPressed = 1;
                    }
                    if (hotkeyPressed == 0 && ev.number == JOY_PLUS) {
                        newVolume = currentVolume + VOL_INC;
                        newVolume = (newVolume > VOL_MAX) ? VOL_MAX : newVolume;
                        break;
                    }
                    if (hotkeyPressed == 0 && ev.number == JOY_MINUS) {
                        newVolume = currentVolume - VOL_INC;
                        newVolume = (newVolume < VOL_MIN) ? VOL_MIN : newVolume;
                        break;
                    }
                } else if (ev.value == 0 && ev.number == JOY_HOTKEY) {
                    hotkeyPressed = 0;
                }
            case JS_EVENT_AXIS:
                if (ev.value == 32767) {
                    if (hotkeyPressed == 1 && ev.number == JOY_DOWN) {
                        execute_mixer(1, VOL_SPK_SWITCH, "off");
                        break;
                    }
                }
            default:
                break;
        }

        if (hotkeyPressed == 0 && newVolume != currentVolume) {
            save_volume(newVolume);
            execute_mixer(0, VOL_SPK_MASTER, &newVolume);
            execute_mixer(1, VOL_SPK_SWITCH, (newVolume == 0) ? "off" : "on");
        }

        usleep(180000);
    }
}

void open_joystick_device() {
    js_fd = open(JOY_DEVICE, O_RDONLY | O_NONBLOCK);

    if (js_fd == -1) {
        perror("Failed to open joystick device");
        exit(1);
    }
}

void restore_volume_settings() {
    int volumeDefault = VOL_DEF;
    int volumeFromFile;
    int volumeSpeakerMax = 255;

    execute_mixer(0, VOL_SPK_LEFT,  &volumeSpeakerMax);
    execute_mixer(0, VOL_SPK_RIGHT, &volumeSpeakerMax);

    FILE *file = fopen(VOL_RST_FILE, "r");
    if (file != NULL) {
        fscanf(file, "%d", &volumeFromFile);
        fclose(file);
        volumeFromFile = (volumeFromFile > VOL_MAX) ? VOL_MAX : volumeFromFile;
        execute_mixer(0, VOL_SPK_MASTER, &volumeFromFile);
    } else {
        execute_mixer(0, VOL_SPK_MASTER, &volumeDefault);
    }
}

int main() {
    restore_volume_settings();
    open_joystick_device();
    read_joystick_events();
}
