---
title: Cores and Configs
description: What RetroArch cores the muOS CFW currently uses for different ROMs.
hide:
  - toc
---

There are a number of cores that are supported on the RG35XX, some better than others. I'll try my best to explain the reasoning behind my choice of core with ROMs.

With the configs that are listed in a different section they are, in my opinion, the best that works for my individual handheld. Hopefully they work nice for you too.

There is also the matter of aspect ratio. I prefer to use the whole screen so streching the screen does not bother me none.

## Importing ROMs

Unlike GarlicOS you don't need to place ROMs into a special directory. For each system create a new directory inside the `ROMS` directory. Then place all the specific ROMs inside that new directory.

You can normally do an automatic scan of ROMs but I find that it's a hit or miss. It's best to do a manual scan with the right core selected. If ROMs are added or removed you can update the playlist from the configuration menu.

## Finding ROMs

There are quite a number of different places on the internet that you can find ROMs. To avoid any legal issues here, I'm not going to link to any websites directly. *Reading comprehension is quite useful.*

If you like fan translations or romhacks perhaps **CDs** and **Romance** are what you seek. Maybe an **Archive** organisation maybe what you are looking for. Also **Directories** that are **Open** are known to be quite useful.

## RetroArch Cores

You should see a list of cores that I have extensively tested. It may be updated as time goes on however I do like to keep my handheld somewhat minimal when it comes to cores and ROMs. In each section I have listed what cores I have used, and also the configurations for each core. Of course all of these can be changed to whatever suits you.