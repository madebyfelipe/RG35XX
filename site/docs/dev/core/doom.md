---
title: Doom (Classic)
description: What the muOS CFW currently uses to run Doom (Classic) ROMs.
hide:
  - toc
---

An absolute classic; I'll never not play this game on whatever platform _(maybe not the Sega 32X version)_.

You'll need `prboom.wad` in the `BIOS` directory. You'll also need to source a bunch of `IWADS` of any of the doom versions. See the following tree on how to set up the DOOM directory in `ROMS` .

Sigil does work, just make sure to use the `-compat` version. The `htp` and `pg` directories are level megawad packs. You can play most of the ones from this URL: [https://doomwiki.org/wiki/Megawad](https://doomwiki.org/wiki/Megawad)

```
┌── doom
│   └── DOOM.WAD
├── doom2
│   └── DOOM2.WAD
├── htp
│   ├── DOOM2.WAD
│   └── HTP.WAD
├── pg
│   ├── DOOM2.WAD
│   └── PG.WAD
├── plutonia
│   └── PLUTONIA.WAD
├── sigil
│   ├── DOOM.WAD
│   └── SIGIL.WAD
└── tnt
    └── TNT.WAD
```

## Configuration

The following should be saved as `PrBoom.opt` in the same folder name under `.retroarch/config`.

```
prboom-analog_deadzone = "5"
prboom-find_recursive_on = "disabled"
prboom-mouse_on = "disabled"
prboom-purge_limit = "32"
prboom-resolution = "640x400"
prboom-rumble = "disabled"
```