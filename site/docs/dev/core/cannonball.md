---
title: Cannonball
description: What the muOS CFW currently uses to run Cannonball.
hide:
  - toc
---

For this core to work a set of roms will need to go into the `Cannonball` directory in `ROMS`.

You can find a list of files at: [https://github.com/djyt/cannonball/blob/master/roms/roms.txt](https://github.com/djyt/cannonball/blob/master/roms/roms.txt)

## Configuration

The following should be saved as `Cannonball.opt` in the same folder name under `.retroarch/config`.

```
cannonball_analog = "OFF"
cannonball_cont_traffic = "3"
cannonball_dip_time = "Easy (80s)"
cannonball_dip_traffic = "Normal"
cannonball_fix_bugs = "ON"
cannonball_fix_timer = "ON"
cannonball_force_ai = "OFF"
cannonball_freeplay = "ON"
cannonball_gear = "Automatic"
cannonball_haptic_strength = "10"
cannonball_jap = "ON"
cannonball_layout_debug = "OFF"
cannonball_level_objects = "ON"
cannonball_menu_enabled = "OFF"
cannonball_menu_road_scroll_speed = "50"
cannonball_new_attract = "ON"
cannonball_pedal_speed = "5"
cannonball_prototype = "ON"
cannonball_randomgen = "ON"
cannonball_sound_advertise = "OFF"
cannonball_sound_enable = "ON"
cannonball_sound_fix_samples = "ON"
cannonball_sound_preview = "OFF"
cannonball_steer_speed = "5"
cannonball_ttrial_laps = "3"
cannonball_ttrial_traffic = "3"
cannonball_video_fps = "Smooth (60)"
cannonball_video_hires = "OFF"
cannonball_video_widescreen = "OFF"
```