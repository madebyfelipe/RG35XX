---
title: DOSBox Pure
description: What the muOS CFW currently uses to run DOSBox Pure ROMs.
hide:
  - toc
---

DOSBox Pure is the go to core for emulating all DOS games. It's a little bit tricky to set up but once you've created your playlist with a couple of games you won't want to go back to any other system... _maybe_.

It's best to install a version of DOSBox locally on your own computer and configure the game correctly up before converting it over to the RG35XX. I won't go into detail here about how to do that, consult your local internet guide or Youtube video.

Once you've configured a game, move all of the game files and directories into a `GAME` directory. Beside the `GAME` directory create a `DOSBOX.CONF` file and inside that file you'll have something along the lines of the following:

```
[AUTOEXEC]
CD GAME
KEEN4.EXE
EXIT
```

For example take the game Commander Keen 4 (which is a fantastic game by the way) and the directory tree should look like the following:

```
┌── DOSBOX.CONF
└── GAME
    ├── AUDIO.CK4
    ├── CONFIG.CK4
    ├── EGAGRAPH.CK4
    ├── GAMEMAPS.CK4
    └── KEEN4.EXE
```

Compress both the `DOSBOX.CONF` and `GAME` objects into a single compressed **.ZIP** file and rename it to something along the lines of `CKEEN4.DOSZ` as an example. You'll then need to place it inside a directory, perhaps `DOS`, under the `ROMS` directory.

You should then be able scan the files and have dosbox-pure as the core to run with the configuration below.

## Configuration

The following should be saved as `DOSBox-pure.opt` in the same folder name under `.retroarch/config`.

```
dosbox_pure_aspect_correction = "false"
dosbox_pure_audiorate = "44100"
dosbox_pure_auto_mapping = "true"
dosbox_pure_auto_target = "0.9"
dosbox_pure_bind_unused = "true"
dosbox_pure_bootos_dfreespace = "1024"
dosbox_pure_bootos_forcenormal = "false"
dosbox_pure_bootos_ramdisk = "false"
dosbox_pure_cga = "early_auto"
dosbox_pure_conf = "inside"
dosbox_pure_cpu_core = "auto"
dosbox_pure_cpu_type = "auto"
dosbox_pure_cycle_limit = "1.0"
dosbox_pure_cycles = "max"
dosbox_pure_cycles_scale = "1.0"
dosbox_pure_force60fps = "true"
dosbox_pure_gus = "false"
dosbox_pure_hercules = "white"
dosbox_pure_joystick_analog_deadzone = "15"
dosbox_pure_joystick_timed = "true"
dosbox_pure_keyboard_layout = "us"
dosbox_pure_latency = "default"
dosbox_pure_machine = "svga"
dosbox_pure_memory_size = "16"
dosbox_pure_menu_time = "5"
dosbox_pure_menu_transparency = "50"
dosbox_pure_midi = "disabled"
dosbox_pure_mouse_input = "true"
dosbox_pure_mouse_speed_factor = "1.0"
dosbox_pure_mouse_speed_factor_x = "1.0"
dosbox_pure_mouse_wheel = "67/68"
dosbox_pure_on_screen_keyboard = "true"
dosbox_pure_perfstats = "none"
dosbox_pure_savestate = "on"
dosbox_pure_sblaster_adlib_emu = "default"
dosbox_pure_sblaster_adlib_mode = "auto"
dosbox_pure_sblaster_conf = "A220 I7 D1 H5"
dosbox_pure_sblaster_type = "sb16"
dosbox_pure_svga = "svga_s3"
dosbox_pure_voodoo = "12mb"
dosbox_pure_voodoo_perf = "1"
```