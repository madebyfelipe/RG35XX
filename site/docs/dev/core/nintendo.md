---
title: Nintendo Systems
description: What the muOS CFW currently uses to run Nintendo System ROMs.
hide:
  - toc
---

There are three different cores all for all of the Nintendo based systems. I've opted to use gpSP for Game Boy, QuickNES for the original NES system, and the 2002 version of Snes9x for SNES systems.

So many different titles from these systems, this is one that I have yet to go through all of them properly.  But from my extensive testing of around 40 popular titles all of the associated cores are, what I find to be, the best of the bunch. You might have some luck with other cores, but hey, that's for you to decide.

## Configuration - Game Boy (DMG/Advance/Color)

The following should be saved as `gpSP.opt` in the same folder name under `.retroarch/config`.

```
gpsp_bios = "official"
gpsp_boot_mode = "game"
gpsp_color_correction = "enabled"
gpsp_drc = "enabled"
gpsp_frame_mixing = "enabled"
gpsp_frameskip = "disabled"
gpsp_frameskip_interval = "1"
gpsp_frameskip_threshold = "33"
gpsp_save_method = "gpSP"
gpsp_turbo_period = "4"
```

## Configuration - NES/Famicon

The following should be saved as `QuickNES.opt` in the same folder name under `.retroarch/config`.

```
quicknes_aspect_ratio_par = "PAR"
quicknes_audio_eq = "tv"
quicknes_audio_nonlinear = "linear"
quicknes_no_sprite_limit = "enabled"
quicknes_palette = "smooth-fbx"
quicknes_turbo_enable = "none"
quicknes_turbo_pulse_width = "3"
quicknes_up_down_allowed = "enabled"
quicknes_use_overscan_h = "disabled"
quicknes_use_overscan_v = "disabled"
```

## Configuration - SNES/SFC

The following should be saved as `Snes9x 2002.opt` in the same folder name under `.retroarch/config`.

```
snes9x2002_frameskip = "disabled"
snes9x2002_frameskip_interval = "1"
snes9x2002_frameskip_threshold = "33"
snes9x2002_low_pass_filter = "enabled"
snes9x2002_low_pass_range = "60"
snes9x2002_overclock_cycles = "disabled"
snes9x2002_transparency = "enabled"
```