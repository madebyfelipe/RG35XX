---
title: PC Engine / CD
description: What the muOS CFW currently uses to run PC Engine / CD ROMs.
hide:
  - toc
---

Personally I'm not too familiar with this system however it does have quite a number of enjoyable titles that can be emulated.

Again, you'll need the right files in the `BIOS` directory for it to run. I've chosen the Beetle PCE Fast core since all of the other cores had freezing, crashing, or other weird issues. Haven't had a title go wrong with this core yet.

## Configuration

The following should be saved as `Beetle PCE Fast.opt` in the same folder name under `.retroarch/config`.

```
pce_fast_adpcmvolume = "100"
pce_fast_cdbios = "System Card 3 US"
pce_fast_cddavolume = "100"
pce_fast_cdimagecache = "enabled"
pce_fast_cdpsgvolume = "100"
pce_fast_cdspeed = "2"
pce_fast_disable_softreset = "enabled"
pce_fast_frameskip = "disabled"
pce_fast_frameskip_threshold = "33"
pce_fast_hoverscan = "352"
pce_fast_initial_scanline = "3"
pce_fast_last_scanline = "242"
pce_fast_mouse_sensitivity = "1.25"
pce_fast_nospritelimit = "enabled"
pce_fast_ocmultiplier = "1"
pce_fast_palette = "Composite"
pce_fast_turbo_delay = "3"
pce_fast_turbo_toggle_hotkey = "disabled"
pce_fast_turbo_toggling = "disabled"
```