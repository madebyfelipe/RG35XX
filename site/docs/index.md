---
title: muOS Introduction
description: So you've decided to try out the muOS custom firmware for the Anbernic RG35XX handheld.
hide:
  - navigation
  - toc
---

![muOS Logo](assets/muos.jpg){ width="196" }

# muOS
So you've decided to try out the muOS custom firmware for the Anbernic RG35XX handheld. This is a personal hobby project of mine and I hope that you'll enjoy it too. **It's RetroArch pure and simple.** I love RetroArch and its simplicity to get my favourite games up and running with little effort. I'm hoping that all of the development I put into this make it as enjoyable as I have had creating it.

## Download latest muOS
Download the latest image here and use your favourite method of writing it to an SD card.

[:material-cloud-download-outline: muOS 2308u-pr2 Full Image](https://mega.nz/file/dnZywTxZ#VUeVs13Qb5oCbPYPZCwAgLG2V1Q62Ntv21Gz6u5JoTM){ .md-button .md-button--primary }

Once you've done the above and started your device at least once for it to initialise, download the following `update.7z` file, when available, and place it on the `muOS BOOT` drive and put the SD card back in your device for it to update to latest changes.

[:material-cloud-download-outline: No Update Available](#){ .md-button .md-button--primary }

## Join the muOS conversation
Have you got suggestions, fixes, bug reports, or just want to talk about stuff and things? Come join us in the muOS Ramble thread on the Retro Handhelds server on Discord.

[:simple-discord: Click to join Discord thread](https://discordapp.com/channels/741895796315914271/1094577589076643840){ .md-button .md-button--primary }

## Special Thanks
* My wonderful wife for putting up with me during this whole project.
* [Retro Handhelds](https://retro-handhelds.com) for the wonderful community that they have created.