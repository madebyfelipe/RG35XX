---
title: Installation
description: How to install muOS on an SD card with explanation of the muOS directory layout.
---

muOS is directed at those who understand, and just want to run, RetroArch. I will try to make it somewhat easy to understand how to use muOS. Stick with GarlicOS if you don't like tinkering with what is already a great CFW choice.

## Requirements

At least a 1GB SD Card.

## Obtaining muOS

Grab the latest muOS SD Card Image from the Super Archive.

[https://archive.xonglebongle.com](https://archive.xonglebongle.com)

Extract the file and use your favourite method of imaging.

## Explanation of muOS Layout

One drive is `BOOT` and the other is the `ROMS`.

The following is a description of each of the folders within the `ROMS` partition:

* `BIOS` Place all of your bios files in here.
* `CORE` Here lies all of the Retroarch cores.
* `IMGS` Thumbnails for various systems go into this directory.
* `LIST` Playlist files that connect everything together.
* `LOGS` Location of RetroArch play time logs.
* `MUOS` This is where muOS resides with all the configs within.
* `PORT` Instead of placing ports into the ROMS folder use this one instead.
* `ROMS` Place your roms in here, no need to worry about directory names.
* `SAVE` Here you will find all of the save files and save states.

## Updating muOS

When available, copy the `update.7z` file to the BOOT partition and restart your device.