---
title: Screenshots
description: Various screenshots of RetroArch and muOS Extras.
hide:
  - navigation
  - toc
---

### RetroArch
Here are a few screenshots of what modifications have been done to RetroArch (RGUI) for muOS.
=== "CPU Performance"
    ![CPU Performance](assets/retroarch/0.png){ width="320" }
=== "RetroArch Settings"
    ![RetroArch Settings](assets/retroarch/1.png){ width="320" }
=== "Playlist Thumbnail"
    ![Playlist Thumbnail](assets/retroarch/2.png){ width="320" }
=== "Composite Filtering"
    ![Composite Filtering](assets/retroarch/3.png){ width="320" }

### muOS Extras
There are more detailed explanations of muOS Extras [here](extra.md).
=== "Main Screen"
    ![Main Screen](assets/extra/0.png){ width="320" }
=== "Date and Time"
    ![Date and Time](assets/extra/1.png){ width="320" }
=== "Utilities"
    ![Utilities](assets/extra/2.png){ width="320" }