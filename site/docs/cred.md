---
title: Credits
description: Thank you to everybody who has supported muOS through support, ideas, feedback and also those talented homebrew developers!
hide:
  - navigation
  - toc
---

!!! tip "muOS Development"
    I would like to express my gratitude to all the folk who contributed to the development, support, ideas, feedback, and of course humour to muOS:

    * acmeplus
    * arynickson
    * Black-Seraph
    * COLESLAW117
    * e1000
    * Joe Staff
    * lepardion
    * poetry
    * porevit
    * qpla
    * XQuader

    And also a big thank you to all of the muOS users! Thank you all for providing suggestions, sharing knowledge, reporting bugs, or participating in ramblings.

    _If you're not on this list, and you think you should be, please accept my apologies and reach out and I will put you on this list._

!!! tip "Homebrew Games"
    These are all of the developers and publishers who were nice enough to have their hard work and creative talents showcased in muOS. All of the homebrew games that come pre-installed with muOS are from the following people. A huge thanks to the following:

    * [Apotris - Developed By: akouzoukos](https://akouzoukos.com/apotris)
    * [Glory Hunters: Chapter One DEMO - Developed By: 2think design studio](https://2think.itch.io)
    * [The Convoy - Developed By: FG Software](https://vectrex28.itch.io)
    * [Blazing Blocks - Developed By: FG Software](https://vectrex28.itch.io)
    * [Renegade Rush - Developed By: quinnp](https://quinnp.itch.io)
    * [Pockettohiro!: DEMO - Developed By: David Marin](https://davidmarin.itch.io/)
    * [Opossum Country - Developed By: Ben Jelter](https://benjelter.itch.io/)
    * [Decline - Developed By: Ben Jelter](https://benjelter.itch.io/)
    * [Unearthed - Developed By: Ben Jelter](https://benjelter.itch.io/)
    * [The Machine: DEMO - Developed By: Ben Jelter](https://benjelter.itch.io/)
    * [Goodboy Galaxy: DEMO - Developed By: Rik Nicol + Jeremy Clarke](https://goodboygalaxy.com/)
    * [Underground Adventure - Developed By: Dale Coop](https://dale-coop.itch.io/)
    * [Press Start Game - Developed By: Dale Coop](https://dale-coop.itch.io/)
    * [Press Start Again - Developed By: Dale Coop](https://dale-coop.itch.io/)
    * [Chibi Monster Brawl: DEMO - Developed By: Dale Coop](https://dale-coop.itch.io/)
    * [Kubo 3 - Developed By: SJ Games](https://dale-coop.itch.io/)
    * [Rescue Time - Developed By: SJ Games](https://dale-coop.itch.io/)
    * [SkateCat: DEMO - Developed By: SJ Games](https://dale-coop.itch.io/)

    _If you are a homebrew developer and would like to showcase your work with muOS please get in touch!_