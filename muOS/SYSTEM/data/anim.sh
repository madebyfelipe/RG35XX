#!/system/bin/sh

# Grab the variables from the following file
. /misc/options.txt

# Start the boot animation process
if [ "$SILENT" = false ]; then
	if [ "$FACTORYRESET" = false ] && [ "$RETRORESET" = false ]; then
		if [ -d /misc/anim ]; then
			THM=/misc/anim/"$THEME"
			if [ -f "$THM"/config.txt ]; then
				echo 1 > /tmp/animwait

				# Grab the configuration of the theme specified
				. "$THM"/config.txt

				echo "$BRIGHTNESS" > /sys/class/backlight/backlight.2/brightness

				if [ "$RUMBLE" = true ]; then
					rumble -s "$RUMBLE_STRENGTH" -t "$RUMBLE_TIME" &
				fi

				if [ "$SOUND" = true ]; then
					MP3C=$(busybox find "$THM/chime" -type f -name "*.mp3" | busybox wc -l)
					CHIME="$THM/chime/$((RANDOM % MP3C)).mp3"
					mp3play "$CHIME" &
				fi

				busybox seq 1 "$LOOP" | while read -r _; do
					for F in $(busybox seq 1 "$END_FRAME"); do
						fbim "$THM/image/$F".png &
						busybox sleep "$TIMING"
					done
				done

				busybox sleep "$SLEEP"
				echo 0 > /tmp/animwait
			fi
		fi
	fi
fi
