#!/system/bin/sh

# Start animation
/system/data/anim.sh &

# Grab the variables from the following file
. /misc/options.txt

# The MUOS root directory
MUOS=/mnt/mmc/MUOS
CDIR=$(pwd)

# Set /misc to read write
mount -o remount,rw /misc

# Disable CPU hotplug
echo 0xF > /sys/devices/system/cpu/autoplug/plug_mask

# Switch to the performance governor for a boot boost
echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor

# Switch USB into device mode and enable ADB if needed (for developers)
if [ "$ADB" = true ]; then /usbdbg.sh device; fi

# Switch the backlight on because of reasons
echo 0 > /sys/class/backlight/backlight.2/bl_power

# Copy the battery DTB over to /misc
cp /misc/dtbs/"$BATTERY".dtb /misc/kernel.dtb

# Configure ALSA symlinks since the system placed them in weird locations
mkdir /usr/share && mkdir /usr/share/alsa
for a in alsa.conf cards pcm; do
	ln -s /system/usr/share/alsa/"$a" /usr/share/alsa/"$a" &
done

# Function to use for updating and regular booting
mount_muos() {
	MUOS_IMAGE=/system/data/muos.img
	MUOS_MOUNTPOINT=/cfw
	LOOPDEVICE=/dev/block/loop7

	# Mount the uClibc-based muOS filesystem
	mkdir $MUOS_MOUNTPOINT
	busybox losetup $LOOPDEVICE $MUOS_IMAGE
	mount -r -w -o loop -t ext4 $LOOPDEVICE $MUOS_MOUNTPOINT

	# Create mount points
	for m in mmc sdcard; do
		mkdir $MUOS_MOUNTPOINT/mnt/"$m"
	done

	# Mount system devices and external SD cards
	for d in dev dev/pts proc sys run mnt/mmc mnt/sdcard; do
		mount -o bind /"$d" $MUOS_MOUNTPOINT/"$d"
	done
}

# Function for displaying the progress bar
progress() {
	busybox killall psplash
	rm psplash_fifo
	psplash -n &
	psplash-write "MSG $2"		#
	psplash-write "MSG $2"		# Doing it twice is not a mistake...
	psplash-write "PROGRESS $1"	#
	psplash-write "PROGRESS $1"
	if [ "$1" -gt 0 ]; then sleep 5; fi
}

# First time booting or just fucked up? Time for a factory reset!
if [ "$FACTORYRESET" = true ]; then
	mount -o remount,rw /system
	umount /mnt/mmc
	progress 0 "start"
	joyaudio > /dev/null 2>&1 &
	joybright > /dev/null 2>&1 &
	mp3play /system/data/reset.mp3 &
	progress 5 "PLEASE WAIT..."
	progress 15 "EXPAND SD CARD"
	busybox printf "fix\n" | parted ---pretend-input-tty /dev/block/mmcblk0 print
	parted ---pretend-input-tty /dev/block/mmcblk0 resizepart 3 100%
	progress 45 "INIT ROM DRIVE"
	mkfs.fat /dev/block/mmcblk0p3
	dosfslabel /dev/block/mmcblk0p3 ROMS
	progress 65 "SET ROM DRIVE FLAGS"
	parted ---pretend-input-tty /dev/block/mmcblk0 set 3 boot off
	parted ---pretend-input-tty /dev/block/mmcblk0 set 3 hidden off
	progress 75 "REBUILD ROM SYSTEM"
	mount -t vfat /dev/block/mmcblk0p3 /mnt/mmc
	cd /mnt/mmc && 7za x /system/data/system.7z && cd "$CDIR" || exit
	progress 90 "ROM DRIVE SYNC"
	gawk -F "=" '/FACTORYRESET/ {sub(/true/, "false", $2)} 1' OFS="=" /misc/options.txt > /misc/temp
	mv /misc/temp /misc/options.txt
	sync
	progress 100 "PRESS RESET BUTTON"
	while true; do sleep 5; done
fi

# Time to reset all of RetroArch settings and configs
if [ "$RETRORESET" = true ]; then
	mount -o remount,rw /system
	umount /mnt/mmc
	progress 0 "start"
	joyaudio > /dev/null 2>&1 &
	joybright > /dev/null 2>&1 &
	mp3play /system/data/reset.mp3 &
	progress 5 "PLEASE WAIT..."
	progress 25 "RESTORING RETROARCH"
	rm -rf /mnt/mmc/MUOS/muos.cfg
	rm -rf /mnt/mmc/MUOS/.retroarch
	progress 50 "RESTORING RETROARCH"
	cd /mnt/mmc/MUOS && 7za x /system/data/retroarch.7z && cd "$CDIR" || exit
	progress 75 "ROM DRIVE SYNC"
	gawk -F "=" '/RETRORESET/ {sub(/true/, "false", $2)} 1' OFS="=" /misc/options.txt > /misc/temp
	mv /misc/temp /misc/options.txt
	sync
	progress 100 "PRESS RESET BUTTON"
	while true; do sleep 5; done
fi

# Time to update the system! This should update all partitions if required.
if [ -f /misc/update.7z ]; then
	progress 0 "start"
	joyaudio > /dev/null 2>&1 &
	joybright > /dev/null 2>&1 &
	mp3play /system/data/reset.mp3 &
	progress 5 "PLEASE WAIT..."
	mount_muos
	mount -o remount,rw /system
	progress 65 "UPDATING SYSTEM"
	cd / && 7za x -aoa /misc/update.7z && cd "$CDIR" || exit
	if [ -f /mnt/mmc/update.sh ]; then
		progress 75 "EXEC UPDATE SCRIPT"
		./mnt/mmc/update.sh
	fi
	rm /misc/update.7z
	rm /mnt/mmc/update.sh
	progress 90 "ROM DRIVE SYNC"
	sync
	progress 100 "PRESS RESET BUTTON"
fi

# Disable the IO scheduler (it just ends up slowing down the MicroSD cards)
echo noop > /sys/devices/b0238000.mmc/mmc_host/mmc0/emmc_boot_card/block/mmcblk0/queue/scheduler
echo noop > /sys/devices/b0230000.mmc/mmc_host/mmc1/sd_card/block/mmcblk1/queue/scheduler

# Disable MicroSD card power-saving (helps reduce load stutter)
echo on > /sys/devices/b0238000.mmc/mmc_host/mmc0/power/control
echo on > /sys/devices/b0230000.mmc/mmc_host/mmc1/power/control

# Mount the second MicroSD card
BLK=/dev/block/mmcblk
mkdir /mnt/sdcard
if [ -e "$BLK"1p1 ]; then SDCARD_DEVICE="$BLK"1p1; else SDCARD_DEVICE="$BLK"1; fi

# Try FAT32 mounting first
F32M=$(mount -t vfat -o rw,utf8,noatime $SDCARD_DEVICE /mnt/sdcard)
if ! $F32M; then mount -t ext4 -o rw,utf8,noatime $SDCARD_DEVICE /mnt/sdcard; fi

# Mount the uClibc-based muOS filesystem from the above function
mount_muos

# Set local variables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:"$PATH"
export HOME=$MUOS
export SHELL=/bin/sh
export SDL_NOMOUSE=1

cd $HOME || exit

if [ "$SCREENSHOT" != false ]; then
	busybox chroot $MUOS_MOUNTPOINT /usr/bin/screenshot "$SCREENSHOT"
fi

# Debug things!
if [ "$HALTBOOT" = true ]; then
	echo 1 > /sys/class/backlight/backlight.2/bl_power
	while true; do sleep 5; done
fi

# Start the low battery indicator
/system/data/lowbatt.sh &

# Start the watchdog process which looks out for the following:
# joyaudio, joybright, joysleep
busybox chroot $MUOS_MOUNTPOINT /usr/bin/watchdog &

# Start JoyIdle with settings from config
busybox chroot $MUOS_MOUNTPOINT /usr/bin/joyidle "$BLANK" "$SHUTDOWN" &

# Set the starting action for the device
ACT=$MUOS/.action
echo "$STARTUP" > $ACT

# Wait for the animation to finish if there is one!
if [ -e /tmp/animwait ]; then
	while true; do
		if [ "$(cat /tmp/animwait)" -eq 0 ]; then
			rm /tmp/animwait
			break
		fi
	done
fi

# Symlink bin for muosutil
rm -rf /bin
ln -s /system/bin /bin

while true; do
	# Back to normal governor
	echo userspace > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor

	# Clear the frame buffer by writing zeroes
	dd if=/dev/zero of=/dev/fb0 bs="$(cat /sys/class/graphics/fb0/size)" count=1 >/dev/null 2>&1

	ACTION=$(cat $ACT)
	if [ -n "$ACTION" ]; then
		case "$ACTION" in
			"retroarch")
				busybox chroot "$MUOS_MOUNTPOINT" /bin/nice -n 19 "$HOME/retroarch" -c "$HOME/muos.cfg"
				;;
			"muosutil")
				echo retroarch > $ACT
				/system/data/muosutil
				;;
			"runport")
				echo retroarch > $ACT
				PORT=$(busybox chroot "$MUOS_MOUNTPOINT" /usr/bin/awk 'NR==1 { print }' < "$HOME/.runport")
				busybox chroot "$MUOS_MOUNTPOINT" "$PORT"
				;;
			"shuffle")
				echo retroarch > $ACT
				SHUFFLE=$(busybox chroot "$MUOS_MOUNTPOINT" /usr/bin/shuffle)
				CORE=$(echo "$SHUFFLE" | busybox sed -n '1p')
				ROM=$(echo "$SHUFFLE" | busybox sed -n '2p')
				busybox chroot "$MUOS_MOUNTPOINT" "$HOME/retroarch" -c "$HOME/muos.cfg" -L "$CORE" "$ROM"
				;;
			"shutdown")
				sync
				busybox poweroff -f
				;;
		esac
	fi
done
