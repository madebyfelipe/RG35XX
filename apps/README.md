## RG35XX Applications

All of the following applications should work on GarlicOS.

* `pdf_reader` - PDF Reader application using fbpdf.
* `rom_shuffle` - Play a random ROM to reduce choice paralysis.

Ready downloads are at: https://archive.xonglebongle.com/
